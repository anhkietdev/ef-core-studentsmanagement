*** ỨNG DỤNG QUẢN LÝ SINH VIÊN ***

Luyện tập cách hiện thực một ứng dụng quản lý sinh viên cho trường học. Dưới đây là các chức năng:

1. Thêm Sinh viên: Cho phép nhập tên, tuổi và ngành học của sinh viên mới. 
2. Hiển thị Danh sách Sinh viên: Hiển thị danh sách tất cả sinh viên với tên, tuổi và ngành học. 
3. Chỉnh Sửa Sinh viên: Cho phép thay đổi tên, tuổi hoặc ngành học của sinh viên. 
4. Xóa Sinh viên: Cung cấp khả năng xóa sinh viên khỏi cơ sở dữ liệu. 
5. Tìm kiếm Sinh viên: Nhập tên sinh viên để tìm và hiển thị danh sách kết quả phù hợp. 
6. Thống kê Sinh viên: Hiển thị tổng số sinh viên, % nam và % nữ. 

