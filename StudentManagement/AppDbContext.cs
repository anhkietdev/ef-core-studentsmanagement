﻿using Microsoft.EntityFrameworkCore;

public class AppDbContext : DbContext
{
    public DbSet<Student> Students { get; set; }
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        var connect = "Data Source=KIETTA;Database=StudentManagement;Trusted_Connection=True;TrustServerCertificate=True;User Id=kietta; Password=123";
        optionsBuilder.UseSqlServer(connect);
    }
}
