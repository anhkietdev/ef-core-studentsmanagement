﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement
{
    public class Menu
    {
        public void Display()
        {
            Console.WriteLine("Student Management");
            Console.WriteLine("------------------");
            Console.WriteLine("1. Add Student");
            Console.WriteLine("2. View Students Infor");
            Console.WriteLine("3. Update Student");
            Console.WriteLine("4. Delete Student");
            Console.WriteLine("5. Find Students By Name");
            Console.WriteLine("6. Statistics");
            Console.WriteLine("7. Exit");
        }

        public void SubMenu()
        {
            Console.WriteLine("1. Update Name");
            Console.WriteLine("2. Update Age");
            Console.WriteLine("3. Update Major");
            Console.WriteLine("4. Update Gender");
            Console.WriteLine("5. Save and Exit ");
        }
    }
}
