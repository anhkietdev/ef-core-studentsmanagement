﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Threading.Channels;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using StudentManagement;

var context = new AppDbContext();
Menu menu = new Menu();

bool exit = false;

while (!exit)
{
    Console.Clear();
    menu.Display();
    Console.Write("Enter your choice: ");
    int choice = int.Parse(Console.ReadLine());
    switch (choice)
    {
        case 1:
            AddNewStudent(context);
            break;
        case 2:
            ViewAllStudent(context);
            break;
        case 3:
            UpdateStudent(context);
            break;
        case 4:
            DeleteStudent(context);
            break;
        case 5:
            FindStudentByName(context);
            break;
        case 6:
            Statistics(context);
            break;
        case 7:
            exit = true;
            break;
        default:
            Console.WriteLine("Invalid choice. Press any key to continue.");
            Console.ReadKey();
            break;
    }
}

void AddNewStudent(AppDbContext context)
{
    Console.Clear();
    Console.WriteLine("Add New Students");
    Console.WriteLine("----------------");
    Console.Write("Enter student name: ");
    string name = Console.ReadLine();
    Console.Write("Enter student age: ");
    int age = int.Parse(Console.ReadLine());
    Console.Write("Enter student major: ");
    string major = Console.ReadLine();
    Console.Write("Enter student gender (M/F): ");
    string gender = Console.ReadLine();
    var student = new Student
    {
        Name = name,
        Age = age,
        Major = major,
        Gender = gender
    };
    context.Students.Add(student);
    context.SaveChanges();
    Console.WriteLine("Student added successfully. Press any key to continue.");
    Console.ReadKey();
}

void ViewAllStudent(AppDbContext context)
{
    Console.Clear();
    Console.WriteLine("View All Students");
    Console.WriteLine("-----------------");

    var students = context.Students.ToList();
    if (students.Count == 0)
    {
        Console.WriteLine("There is no student in list!");
    }
    else
    {
        foreach (var student in students)
        {
            Console.WriteLine($"Name: {student.Name},  Age: {student.Age},  Major: {student.Major},  Gender: {student.Gender}");
        }
    }
    Console.WriteLine("Press any key to continue");
    Console.ReadKey();
}


void UpdateStudent(AppDbContext context)
{
    Console.Clear();
    Console.WriteLine("Update Students");
    Console.WriteLine("-----------------");

    Console.Write("Enter Student ID to update: ");
    int id = int.Parse(Console.ReadLine());

    var student = context.Students.FirstOrDefault(x => x.ID == id);
    if (student == null)
    {
        Console.WriteLine("Invalid ID");
    }
    else
    {
        bool exit = false;
        while (!exit)
        {
            Console.WriteLine("----- Student Infor -----");
            Console.WriteLine($"Name: {student.Name}, Age: {student.Age}, Major: {student.Major}, Gender: {student.Gender}");
            menu.SubMenu();
            Console.WriteLine("Your choice: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    Console.Write("Enter new name: ");
                    student.Name = Console.ReadLine();
                    break;
                case 2:
                    Console.Write("Enter new Age: ");
                    student.Age = int.Parse(Console.ReadLine());
                    break;
                case 3:
                    Console.Write("Enter new Major: ");
                    student.Major = Console.ReadLine();
                    break;
                case 4:
                    Console.Write("Enter new Gender: ");
                    student.Gender = Console.ReadLine();
                    break;
                case 5:
                    context.SaveChanges();
                    exit = true;
                    break;
                default:
                    Console.WriteLine("Invalid choice. Please choose again");
                    Console.ReadKey();
                    break;
            }
        }
    }
    Console.WriteLine("Press any key to continue");
    Console.ReadKey();
}


void DeleteStudent(AppDbContext context)
{
    Console.Clear();
    Console.WriteLine("Delete Students");
    Console.WriteLine("-----------------");

    Console.Write("Enter Student ID to Delete: ");
    int id = int.Parse(Console.ReadLine());
    var student = context.Students.FirstOrDefault(x => x.ID == id);
    if (student == null)
    {
        Console.WriteLine("Invalid ID");
    }
    else
    {
        context.Students.Remove(student);
        context.SaveChanges();
        Console.WriteLine("Student deleted successfully!");
    }
    Console.WriteLine("Press any key to continue");
    Console.ReadKey();
}

void FindStudentByName(AppDbContext context)
{
    Console.Clear();
    Console.WriteLine("Find Students By Name");
    Console.WriteLine("---------------");
    Console.Write("Enter Student's Name to search: ");
    string name = Console.ReadLine();
    var students = context.Students.Where(s => s.Name.Contains(name)).ToList();
    if (students.Count == 0)
    {
        Console.WriteLine("No students found.");
    }
    else
    {
        foreach (var student in students)
        {
            Console.WriteLine($"Id: {student.ID}, Name: {student.Name}, Age: {student.Age}, Major: {student.Major}, Gender: {student.Gender}");
        }
    }
    Console.WriteLine("Press any key to continue.");
    Console.ReadKey();
}

void Statistics(AppDbContext context)
{
    Console.Clear();
    Console.WriteLine("Students Statistic");
    Console.WriteLine("------------------");

    int totalAmount = context.Students.Count();
    int totalMale = context.Students.Count(m => m.Gender == "M");
    int totalFemale = context.Students.Count(m => m.Gender == "F");

    double malePercentage = (double)totalMale / totalAmount * 100;
    double feMalePercentage = (double)totalFemale / totalAmount * 100;

    Console.WriteLine($"Total amount: {totalAmount}");
    Console.WriteLine($"Male percentage: {malePercentage}%");
    Console.WriteLine($"Total amount: {feMalePercentage}%");

    Console.WriteLine("Press any key to continue");
    Console.ReadKey();

}






